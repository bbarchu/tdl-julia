### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ b68736c0-5f4c-11eb-15f5-674f13bb95d3
using Flux, Plots;

# ╔═╡ 9dc53110-5f5f-11eb-11ad-d12ca67229c6
using Tracker

# ╔═╡ 9d5a1100-5f50-11eb-3953-13d150e9f1b8
import Pkg; Pkg.add(["CSV","DataFrames"]); using CSV, DataFrames;

# ╔═╡ d29c3d60-5f4c-11eb-19b9-1d3b5ed29561
σ #\sigma + tab

# ╔═╡ 9f403fb0-5f5c-11eb-2af2-6b09924b7ba3
plot(σ, -5, 5, label="\\sigma", xlabel="x", ylabel="\\sigma\\(x\\)")


# ╔═╡ 62ff83a0-5f5a-11eb-0b54-41c30f61b5fd
model = Dense(2,1,σ) #2 inputs y 1 output, 

# ╔═╡ 11f7bd10-5f46-11eb-0285-698d1e980022
model.W #Peso

# ╔═╡ 87dd896e-5f4f-11eb-34cc-b59fc98e12c3
model.b #Bias/sesgo

# ╔═╡ b53a15f0-5f4f-11eb-1fa2-77b1a447d692
typeof(model.W)

# ╔═╡ 1ffff7a0-5f60-11eb-3213-ef79eb82f3bf
begin
	W = param(model.W)
	b = param(model.b)
end

# ╔═╡ bf21ca90-5f4f-11eb-10b3-ebf936d256cf
x = rand(2); model(x) #model no es una funcion pero puede actuar como una.

# ╔═╡ 29e8b0f0-5f50-11eb-2d97-4544741cbd7d
σ.(W*x + b) #como podemos observar tenemos el mismo resultado (ya que esta es la definicion de la densidad de la neurona)


# ╔═╡ 52efed10-5f50-11eb-0d28-8545ca072e28
#"mean square error"
methods(Flux.mse)

# ╔═╡ 81d5951e-5f51-11eb-0af6-57cb2fb3823b
begin
apples = DataFrame(CSV.File("./apples.dat",delim='\t',normalizenames=true));
bananas = DataFrame(CSV.File("./bananas.dat",delim='\t', normalizenames=true))
end


# ╔═╡ c46b1080-5f52-11eb-2429-b31985ffd84d
begin
	x_apples = [ [row.red, row.green] for row in eachrow(apples)];
	x_bananas = [ [row.red, row.green] for row in eachrow(bananas)];
end

# ╔═╡ 0c1ddca0-5f53-11eb-155b-a5c536012e2f
xs = [x_apples; x_bananas]

# ╔═╡ 18ee0b30-5f53-11eb-3d36-6f2c320c3ab1
ys = [fill(0,size(x_apples)); fill(1,size(x_bananas))]

# ╔═╡ 38b9d840-5f53-11eb-2bfb-bb65ba63bb73
model(xs[end])

# ╔═╡ 758978c2-5f53-11eb-0808-e736eab67055
loss = Flux.mse(model(xs[1]), ys[1])

# ╔═╡ a793fcf0-5f53-11eb-338a-11e7cf025588
typeof(loss)

# ╔═╡ ed2596c0-5f53-11eb-3752-952217db0569
model.W

# ╔═╡ 04ea07f0-5f54-11eb-10df-b39839ae5856
model.b

# ╔═╡ f83d7180-5f54-11eb-1c50-a3e3c6fa541c
back!(loss)

# ╔═╡ Cell order:
# ╠═b68736c0-5f4c-11eb-15f5-674f13bb95d3
# ╠═9dc53110-5f5f-11eb-11ad-d12ca67229c6
# ╠═d29c3d60-5f4c-11eb-19b9-1d3b5ed29561
# ╠═9f403fb0-5f5c-11eb-2af2-6b09924b7ba3
# ╠═62ff83a0-5f5a-11eb-0b54-41c30f61b5fd
# ╠═11f7bd10-5f46-11eb-0285-698d1e980022
# ╠═87dd896e-5f4f-11eb-34cc-b59fc98e12c3
# ╠═b53a15f0-5f4f-11eb-1fa2-77b1a447d692
# ╠═1ffff7a0-5f60-11eb-3213-ef79eb82f3bf
# ╠═bf21ca90-5f4f-11eb-10b3-ebf936d256cf
# ╠═29e8b0f0-5f50-11eb-2d97-4544741cbd7d
# ╠═52efed10-5f50-11eb-0d28-8545ca072e28
# ╠═9d5a1100-5f50-11eb-3953-13d150e9f1b8
# ╠═81d5951e-5f51-11eb-0af6-57cb2fb3823b
# ╠═c46b1080-5f52-11eb-2429-b31985ffd84d
# ╠═0c1ddca0-5f53-11eb-155b-a5c536012e2f
# ╠═18ee0b30-5f53-11eb-3d36-6f2c320c3ab1
# ╠═38b9d840-5f53-11eb-2bfb-bb65ba63bb73
# ╠═758978c2-5f53-11eb-0808-e736eab67055
# ╠═a793fcf0-5f53-11eb-338a-11e7cf025588
# ╠═ed2596c0-5f53-11eb-3752-952217db0569
# ╠═04ea07f0-5f54-11eb-10df-b39839ae5856
# ╠═f83d7180-5f54-11eb-1c50-a3e3c6fa541c
