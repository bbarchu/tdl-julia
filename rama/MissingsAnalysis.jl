### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 3ed1dd2e-55c4-11eb-0a6c-358108743a2f
begin
	import Pkg
	Pkg.add("Plots")
	Pkg.add("CSV")
	Pkg.add("DataFrames")
	Pkg.add("PlutoUI")
	using Plots, CSV, DataFrames, PlutoUI
end

# ╔═╡ 3c70bada-55c6-11eb-28f8-27d379a1cb01
plotly()

# ╔═╡ 84a8c90a-70b8-11eb-22b7-13aedcfb8bdd
md"# Analisis de datos nulos

Carga del dataframe, aprovecho el parametro missingstring:"

# ╔═╡ 82b3da2c-55c6-11eb-1528-f104047175d0
df = DataFrame(CSV.File("weatherAUS.csv";  missingstring="NA"))

# ╔═╡ 83b3dc94-70b8-11eb-3030-53a052e97035
begin
	row_cty = nrow(df)
	description = describe(df)
	description.nmissingPercent = description.nmissing .* 100 ./ row_cty
	description.variable = map(x -> string(x), description.variable)
	sort!(description, [:nmissing], rev = true)
end

# ╔═╡ cc60f02c-55ff-11eb-375b-dffbe2167328
md"# Visualizacion de datos faltantes

Se puede mover el slider de aqui abajo para modificar la cantidad de datos faltantes a evaluar"

# ╔═╡ 9e36c3f0-5604-11eb-3802-ef1dd723ad6f
@bind p Slider(1:23, default = 10)

# ╔═╡ 4a5ccdf2-5605-11eb-2c34-9f97b46d7016
p

# ╔═╡ e775a396-55d0-11eb-1f6c-49de7e0b1770
missing_cty = plot(description.variable[1:p], description.nmissingPercent[1:p], seriestype = :bar, orientation=:h, label=false, yflip=true, title="Top $p cantidad de datos faltantes", xlabel="Porcentaje de datos faltantes", xlim=(0,55))

# ╔═╡ 86cc78e8-567b-11eb-2965-f770bcb048b3
describe(df[!, description.variable[1:p]])

# ╔═╡ 6dd83034-5612-11eb-3e74-fd2a364b5a4b
df2 = df[ismissing.(df.Sunshine) .== false, :]

# ╔═╡ 94188668-5612-11eb-3cec-c1f318233f91
sunshine_without_missing = histogram(df2.Sunshine, label=false, title="Histograma: Sunshine sin missing values", colour="magenta")

# ╔═╡ c64bdd2e-5603-11eb-226f-11e9e5ea6c9c
df3 = copy(df)

# ╔═╡ fab167f6-55d0-11eb-168e-2104f0bf9e2b
replace!(df3.Sunshine, missing => description[1, :mean])

# ╔═╡ a3052a70-5679-11eb-1bd0-25797ccb89ec
df3

# ╔═╡ 75e1b4ea-567f-11eb-231c-cf4e0b08a10c
sunshine_with_mean = histogram(df3.Sunshine, bins=200,  label=false, title="Sunshine completado con el promedio", color = "blue")

# ╔═╡ cfa5dff0-5680-11eb-204c-578d257e9264
df4 = copy(df)

# ╔═╡ 241887fc-70b9-11eb-145e-bf159addf7d7


# ╔═╡ 908d9168-5681-11eb-2150-d3ca6112f3a4
df4[ismissing.(df4.Sunshine) .== true, :Sunshine] = rand(description[1, :min] :0.1: description[1, :max], description[1, :nmissing])

# ╔═╡ 486e9b56-5682-11eb-1db1-b9a3dce678d0
sunshine_with_rand = histogram(df4.Sunshine,  label=false, title="Sunshine completado con numeros random", color="lightblue")

# ╔═╡ 8d531634-5682-11eb-0c2c-73c7f6a240b0
describe(df4[!, [:Sunshine]])

# ╔═╡ 253a66f0-5683-11eb-1fbf-b73e5483bd15
plot(sunshine_without_missing, sunshine_with_mean, sunshine_with_rand, layout = (3, 1))

# ╔═╡ cb4580a0-568a-11eb-0fa0-1d91727fbe6f
df2_1 = copy(df[ismissing.(df.Sunshine) .== true, :])

# ╔═╡ e6b644b4-568a-11eb-2333-eb9c1a8a3511
gd = groupby(df2_1, :Location)

# ╔═╡ a86356ce-568b-11eb-35c5-b902589e16de
gd_result = combine(gd, nrow)

# ╔═╡ d87296f4-568b-11eb-31bf-890bb7024d0b
sort!(gd_result, :nrow, rev=true)

# ╔═╡ f313f6da-568b-11eb-3f88-7b33a584af8a
sunshine_by_city = bar( gd_result.Location, gd_result.nrow,  xrotation=45, label=false, title="Sunshine: Cantidad de valores Missing por ciudad", color="grey")

# ╔═╡ 8af62b8c-568d-11eb-1ce8-cf8bb77a8585
# Variable evaporation

# ╔═╡ 9006ab2e-568d-11eb-29b5-e5b89421e1e8
dfe2 = df[ismissing.(df.Evaporation) .== false, :]

# ╔═╡ c40bfde8-568d-11eb-02fc-a11559770a83
evaporation_without_missing = histogram(dfe2.Evaporation, label=false, title="Histograma: Evaporation sin missing values", xlim=(0,30), color="magenta")

# ╔═╡ e768cce4-568d-11eb-2977-cbd1db4845ab
dfe3 = copy(df)

# ╔═╡ faca9b82-568d-11eb-3b0a-89519d195fb7
replace!(dfe3.Evaporation, missing => description[2, :mean])

# ╔═╡ 02f14d4c-568e-11eb-2f60-add8bb461dd5
evaporation_with_mean = histogram(dfe3.Evaporation, label=false, title="Histograma: Evaporation completado con promedio", xlim=(0,30), color="blue")

# ╔═╡ 6aa3e26e-568f-11eb-2d40-4ff61945056d
dfe4 = copy(df)

# ╔═╡ 89fce322-568f-11eb-1850-917b844389e2
dfe4[ismissing.(dfe4.Evaporation) .== true, :Evaporation] = rand(description[2, :min] :0.1: description[2, :max], description[2, :nmissing])

# ╔═╡ 9cb076c6-568f-11eb-1a86-b3497f4cd590
evaporation_with_rand = histogram(dfe4.Evaporation, label=false, title="Histograma: Evaporation completado con random", color="lightblue")

# ╔═╡ dd095a7a-568f-11eb-257a-d737b005b18d
plot(evaporation_without_missing, evaporation_with_mean, evaporation_with_rand, layout = (3, 1))

# ╔═╡ 0324c6b6-5690-11eb-29b8-ff57bb9adecc
gde = groupby(df[ismissing.(df.Evaporation) .== true, :], :Location)

# ╔═╡ 28032d24-5690-11eb-0025-ddf2f19f0a16
gde_result = combine(gde, nrow)

# ╔═╡ 3d515368-5690-11eb-1de0-b32957d5fcf9
sort!(gde_result, :nrow, rev=true)

# ╔═╡ 468505f8-5690-11eb-169a-d3d53bb40e05
evaporation_by_city = bar( gde_result.Location, gde_result.nrow,  xrotation=45, label=false, title="Evaporation: Cantidad de valores Missing por ciudad")

# ╔═╡ 7c2746b0-5690-11eb-2ae6-1b80b15593c4
plot(sunshine_by_city, evaporation_by_city, layout = (2,1))

# ╔═╡ 7e22337c-5692-11eb-2d2b-af4552d95465
def = copy(df)

# ╔═╡ a25eb9ce-5692-11eb-1598-1f2cb593237e
def[ismissing.(def.Sunshine) .== true, :Sunshine] = rand(description[1, :min] :0.1: description[1, :max], description[1, :nmissing])

# ╔═╡ 33620a6c-707d-11eb-29b8-ff92a16210de
def[ismissing.(def.Evaporation) .== true, :Evaporation] = rand(description[2, :min] :0.1: description[2, :max], description[2, :nmissing])

# ╔═╡ b74b1436-5692-11eb-2e5b-5584d558ff14
describe(def)

# ╔═╡ 03072078-5697-11eb-0c9a-b910be8be403
dfAlb = copy(df[df.Location .== "Albury", :])

# ╔═╡ 39111642-5697-11eb-349c-1babc7342ba2
gb_locations = groupby(df, :Location)

# ╔═╡ 7c755dd6-56a0-11eb-05d7-f5bf3a2410b7
function fn_combine(self_df)
	df_aux = describe(self_df)
	df_transposed = DataFrame([[names(df_aux)]; collect.(eachrow(df_aux))], [:column; Symbol.(axes(df_aux, 1))])
	return df_transposed[df_transposed.column .== "nmissing", :]
end

# ╔═╡ 74918af8-5699-11eb-30b1-3b2240807c07
cbg_locations = combine(gb_locations, fn_combine)

# ╔═╡ 18113d52-569a-11eb-0704-3fe4d04bd277
col_names = names(df)[3:23]

# ╔═╡ e6ffeb54-591f-11eb-2151-cd209ad8d888
locations = unique(df.Location)

# ╔═╡ 24ceb828-5920-11eb-3f10-9d28fcb9617e
hm_data = Matrix(cbg_locations[!, 5:25])

# ╔═╡ bb79aa68-70b9-11eb-0554-9ba657e2e1d6
md"# Heatmap plots sobre cantidad de datos nulos por ciudad"

# ╔═╡ dec31188-5699-11eb-3cfa-e7d9d4c347ce
begin
										#Comparacion con Python:
	heatmaps = Any[]					#heatmaps = []
	i = 1
	j = 1
	while i <= 49
		while j <= 21
			hm_data_part = hm_data[i:i+6, j:j+6]
			xlabel = col_names[j:j+6]
			ylabel = locations[i:i+6]
			heatMap = heatmap(hm_data_part, xticks=(1:7, xlabel), xrotation=45 , yticks=(1:7, ylabel), fill_z=hm_data_part, aspect_ratio=:equal, xtickfont = font(7, "Courier"), ytickfont = font(7, "Courier"), clims=(0,maximum(hm_data)) )	
			push!(heatmaps, heatMap)	#heatmaps.append(...)
			j += 7
		end
		i += 7
		j = 1
	end
end

# ╔═╡ f87276c0-7073-11eb-1cbb-8bf1839f0d9d
plot(heatmaps[4],heatmaps[5],heatmaps[6], heatmaps[1],heatmaps[2],heatmaps[3], layout=(2,3), link = :both)

# ╔═╡ c69a9636-7079-11eb-066e-bb2ebe12faed
plot(heatmaps[10],heatmaps[11],heatmaps[12],heatmaps[7],heatmaps[8],heatmaps[9], layout=(2,3), link = :both)

# ╔═╡ f7f97afc-707a-11eb-2ef6-09e0d304e5ba
plot(heatmaps[19],heatmaps[20],heatmaps[21],heatmaps[16],heatmaps[17],heatmaps[18], layout=(2,3), link = :both)

# ╔═╡ Cell order:
# ╠═3ed1dd2e-55c4-11eb-0a6c-358108743a2f
# ╠═3c70bada-55c6-11eb-28f8-27d379a1cb01
# ╟─84a8c90a-70b8-11eb-22b7-13aedcfb8bdd
# ╠═82b3da2c-55c6-11eb-1528-f104047175d0
# ╠═83b3dc94-70b8-11eb-3030-53a052e97035
# ╟─cc60f02c-55ff-11eb-375b-dffbe2167328
# ╠═9e36c3f0-5604-11eb-3802-ef1dd723ad6f
# ╠═4a5ccdf2-5605-11eb-2c34-9f97b46d7016
# ╠═e775a396-55d0-11eb-1f6c-49de7e0b1770
# ╠═86cc78e8-567b-11eb-2965-f770bcb048b3
# ╠═6dd83034-5612-11eb-3e74-fd2a364b5a4b
# ╠═94188668-5612-11eb-3cec-c1f318233f91
# ╠═c64bdd2e-5603-11eb-226f-11e9e5ea6c9c
# ╠═fab167f6-55d0-11eb-168e-2104f0bf9e2b
# ╠═a3052a70-5679-11eb-1bd0-25797ccb89ec
# ╠═75e1b4ea-567f-11eb-231c-cf4e0b08a10c
# ╠═cfa5dff0-5680-11eb-204c-578d257e9264
# ╠═241887fc-70b9-11eb-145e-bf159addf7d7
# ╠═908d9168-5681-11eb-2150-d3ca6112f3a4
# ╠═486e9b56-5682-11eb-1db1-b9a3dce678d0
# ╠═8d531634-5682-11eb-0c2c-73c7f6a240b0
# ╠═253a66f0-5683-11eb-1fbf-b73e5483bd15
# ╠═cb4580a0-568a-11eb-0fa0-1d91727fbe6f
# ╠═e6b644b4-568a-11eb-2333-eb9c1a8a3511
# ╠═a86356ce-568b-11eb-35c5-b902589e16de
# ╠═d87296f4-568b-11eb-31bf-890bb7024d0b
# ╠═f313f6da-568b-11eb-3f88-7b33a584af8a
# ╠═8af62b8c-568d-11eb-1ce8-cf8bb77a8585
# ╠═9006ab2e-568d-11eb-29b5-e5b89421e1e8
# ╠═c40bfde8-568d-11eb-02fc-a11559770a83
# ╠═e768cce4-568d-11eb-2977-cbd1db4845ab
# ╠═faca9b82-568d-11eb-3b0a-89519d195fb7
# ╠═02f14d4c-568e-11eb-2f60-add8bb461dd5
# ╠═6aa3e26e-568f-11eb-2d40-4ff61945056d
# ╠═89fce322-568f-11eb-1850-917b844389e2
# ╠═9cb076c6-568f-11eb-1a86-b3497f4cd590
# ╠═dd095a7a-568f-11eb-257a-d737b005b18d
# ╠═0324c6b6-5690-11eb-29b8-ff57bb9adecc
# ╠═28032d24-5690-11eb-0025-ddf2f19f0a16
# ╠═3d515368-5690-11eb-1de0-b32957d5fcf9
# ╠═468505f8-5690-11eb-169a-d3d53bb40e05
# ╠═7c2746b0-5690-11eb-2ae6-1b80b15593c4
# ╠═7e22337c-5692-11eb-2d2b-af4552d95465
# ╠═a25eb9ce-5692-11eb-1598-1f2cb593237e
# ╠═33620a6c-707d-11eb-29b8-ff92a16210de
# ╠═b74b1436-5692-11eb-2e5b-5584d558ff14
# ╠═03072078-5697-11eb-0c9a-b910be8be403
# ╠═39111642-5697-11eb-349c-1babc7342ba2
# ╠═7c755dd6-56a0-11eb-05d7-f5bf3a2410b7
# ╠═74918af8-5699-11eb-30b1-3b2240807c07
# ╠═18113d52-569a-11eb-0704-3fe4d04bd277
# ╠═e6ffeb54-591f-11eb-2151-cd209ad8d888
# ╠═24ceb828-5920-11eb-3f10-9d28fcb9617e
# ╟─bb79aa68-70b9-11eb-0554-9ba657e2e1d6
# ╠═dec31188-5699-11eb-3cfa-e7d9d4c347ce
# ╠═f87276c0-7073-11eb-1cbb-8bf1839f0d9d
# ╠═c69a9636-7079-11eb-066e-bb2ebe12faed
# ╠═f7f97afc-707a-11eb-2ef6-09e0d304e5ba
