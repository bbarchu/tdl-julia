### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ d662c326-567b-11eb-0c4e-67d49255184a
begin
	import Pkg
	Pkg.add("Plots")
	Pkg.add("CSV")
	Pkg.add("DataFrames")
	Pkg.add("PlutoUI")
	using Plots, CSV, DataFrames, PlutoUI, Statistics, Dates
end

# ╔═╡ b76b2cba-567b-11eb-0bda-07f959dea5cb
plotly()

# ╔═╡ bd8ed5fe-567b-11eb-2b86-c9f2e97b97ff
df = DataFrame(CSV.File("weatherAUS.csv";  missingstring="NA"))

# ╔═╡ 05ff22e6-7088-11eb-305f-fdcbf49100d3
begin
	description = describe(df)
	row_cty = nrow(df)
	description.nmissingPercent = description.nmissing .* 100 ./ row_cty
	sort!(description, [:nmissing], rev = true)
end

# ╔═╡ f8023b6c-7087-11eb-08d3-47bb8007b4df
description

# ╔═╡ a06a02c8-70b6-11eb-38c9-a9f712960b59
md"# Funcion de limpieza del dataframe

A continuacion limpiaremos el dataframe para luego explorarlo"

# ╔═╡ 29041136-7088-11eb-2fd3-e1e6cdc9210c
function missingClean(self_df)
	#copio el data frame original para no modificarlo
	cleanDf = copy(self_df)
	#elimino los features con mas cantidad de missings
	select!(cleanDf, Not([:Cloud3pm, :Cloud9am, :Pressure9am, :Pressure3pm]))
	#elimino las filas que contangan datos missing de las siguientes features
	dropmissing!(cleanDf, [:WindDir9am, :WindGustDir, :WindGustSpeed, :Humidity3pm, :WindDir3pm, :RainToday, :RainTomorrow, :Humidity9am, :MinTemp, :MaxTemp])
	#relleno con numeros randoms los campos evaporation y sunshine
	description = describe(cleanDf)
	cleanDf[ismissing.(cleanDf.Sunshine) .== true, :Sunshine] = rand(description[description.variable .== Symbol(:Sunshine), :min][1] :0.1: description[description.variable .== Symbol(:Sunshine), :max][1], description[description.variable .== Symbol(:Sunshine), :nmissing][1])
	cleanDf[ismissing.(cleanDf.Evaporation) .== true, :Evaporation] = rand(description[description.variable .== Symbol(:Evaporation), :min][1] :0.1: description[description.variable .== Symbol(:Evaporation), :max][1], description[description.variable .== Symbol(:Evaporation), :nmissing][1])
	return cleanDf
end

# ╔═╡ a1851ce2-7088-11eb-1245-6127f38ed25e
begin
	cleanDf = missingClean(df)
	cleanDescription = describe(cleanDf)
end

# ╔═╡ c503cc88-70b6-11eb-3aad-3b93d0adb235
md"Verificamos la cantidad de datos faltantes en todas las columnas del dataframe"

# ╔═╡ e76b07e2-708d-11eb-053d-23d98dd75dec
size(cleanDescription[cleanDescription.nmissing .!= 0, :nmissing])

# ╔═╡ 5078035c-708e-11eb-27c2-9d2853cc624b
begin
	rows_cty_df = nrow(df)
	rows_cty_cleanDf = nrow(cleanDf)
	lost = 100 - rows_cty_cleanDf * 100 / rows_cty_df
end

# ╔═╡ dd38fd46-70b6-11eb-2ba9-a579bf7ef355
md"Calculamos el porcentaje de filas perdidas: $lost%"

# ╔═╡ 0024f100-567e-11eb-267e-2f0bbd9c8efc
df2 = df[ismissing.(df.Rainfall) .== false, :]

# ╔═╡ f709c0d4-70b6-11eb-35b7-ed71f4923876
md"Histograma de mm de lluvia caidos

Puede mover el slider de aqui abajo para modificar la cantidad maxima de mm de lluvia"

# ╔═╡ c12e76a8-567b-11eb-0b75-45499d045565
@bind lim Slider(20:50, default = 25)

# ╔═╡ c5398bde-567b-11eb-3fc3-fdbb254aead7
lim

# ╔═╡ d018c13c-567b-11eb-238a-edbb9867c78d
histogram(df2.Rainfall, bins = 500, alpha=0.4, label=false, xlims=(0,lim), ylims=(0,8000), title= "Histograma: Cantidad de mm de lluvia", ylabel="Cantidad de ocurrencias, crece hasta 100000 ->", xlabel = "mm de lluvia")

# ╔═╡ d4550724-567b-11eb-0bea-93580221986d
begin
	cleanDf.year = Dates.year.(cleanDf.Date)
	cleanDf.month = Dates.month.(cleanDf.Date)
	cleanDf.day = Dates.day.(cleanDf.Date)
end

# ╔═╡ 50d46c5a-7093-11eb-269a-4122a256876d
gbYearLocation = groupby(cleanDf, [:year, :Location])

# ╔═╡ fe3933da-7093-11eb-3788-3f23ce6a96e0
begin
	cmbYearLocationRain = combine(gbYearLocation, :Rainfall => mean)
	sort!(cmbYearLocationRain, [:year])
end

# ╔═╡ 2e02fa60-70b7-11eb-3332-43bd8ad21151
md"# Promedio de cantidad de lluvia caida por año"

# ╔═╡ cc0e2790-7094-11eb-3555-91d53a0f2791
#Funcion para plotear la cantidad de lluvia caida cada año de cada ciudad
function myPlot()
	locations = unique(cmbYearLocationRain.Location)
	location = locations[1]
	cmbYearRainLocation = cmbYearLocationRain[cmbYearLocationRain.Location .== location, :]
	myRetPlot = plot(cmbYearRainLocation.year, cmbYearRainLocation.Rainfall_mean, label=location, title="Promedio de lluvias en mm", xlabel="año", ylabel="mm de lluvia")
 	locations = locations[2:(size(locations)[1])]
#	locations = locations[2:5]
	for location in locations
		cmbYearRainLocation = cmbYearLocationRain[cmbYearLocationRain.Location .== location, :]
		plot!(cmbYearRainLocation.year, cmbYearRainLocation.Rainfall_mean, label=location)
	end
	return myRetPlot
end

# ╔═╡ 73c94bb8-7098-11eb-3b39-893142b5cc08
plots = myPlot()

# ╔═╡ 38c6ddd4-709b-11eb-22dc-91e11f4922d2
gbYear = groupby(cleanDf, [:year])

# ╔═╡ a1af358a-709b-11eb-194b-e32956b6c33d
begin
	cmbYearRain = combine(gbYear, :Rainfall => mean)
	sort!(cmbYearRain, [:year])
end

# ╔═╡ c1e6dc5e-709b-11eb-2e9f-a91753de7104
agrupado = plot(cmbYearRain.year, cmbYearRain.Rainfall_mean, title="Promedio de lluvia por año", xlabel="año", ylabel="mm de lluvia", label="Agrupado", linewidth = 5)

# ╔═╡ e9d138ae-709b-11eb-38d0-6fdb86e287ee
begin
	combMaxTempYear = combine(gbYear, :MaxTemp=>mean)
	sort!(combMaxTempYear, :year)
	combMinTempYear = combine(gbYear, :MinTemp=>mean)
	sort!(combMinTempYear, :year)
end

# ╔═╡ ecb072fa-70b5-11eb-2eea-3bc9a3836f12
begin
	plot(combMaxTempYear.year, combMaxTempYear.MaxTemp_mean, title="Promedio de temperatura por año", xlabel="año", ylabel="grados", label="Temperatura Maxima", linewidth = 5, color = "RED")
	plot!(combMinTempYear.year, combMinTempYear.MinTemp_mean, title="Temperaturas anuales", xlabel="año", ylabel="grados", label="Temperatura Minima", linewidth = 5, color = "BLUE")
end

# ╔═╡ Cell order:
# ╠═d662c326-567b-11eb-0c4e-67d49255184a
# ╠═b76b2cba-567b-11eb-0bda-07f959dea5cb
# ╠═bd8ed5fe-567b-11eb-2b86-c9f2e97b97ff
# ╠═05ff22e6-7088-11eb-305f-fdcbf49100d3
# ╠═f8023b6c-7087-11eb-08d3-47bb8007b4df
# ╟─a06a02c8-70b6-11eb-38c9-a9f712960b59
# ╠═29041136-7088-11eb-2fd3-e1e6cdc9210c
# ╠═a1851ce2-7088-11eb-1245-6127f38ed25e
# ╟─c503cc88-70b6-11eb-3aad-3b93d0adb235
# ╠═e76b07e2-708d-11eb-053d-23d98dd75dec
# ╟─dd38fd46-70b6-11eb-2ba9-a579bf7ef355
# ╠═5078035c-708e-11eb-27c2-9d2853cc624b
# ╠═0024f100-567e-11eb-267e-2f0bbd9c8efc
# ╟─f709c0d4-70b6-11eb-35b7-ed71f4923876
# ╠═c12e76a8-567b-11eb-0b75-45499d045565
# ╠═c5398bde-567b-11eb-3fc3-fdbb254aead7
# ╠═d018c13c-567b-11eb-238a-edbb9867c78d
# ╠═d4550724-567b-11eb-0bea-93580221986d
# ╠═50d46c5a-7093-11eb-269a-4122a256876d
# ╠═fe3933da-7093-11eb-3788-3f23ce6a96e0
# ╟─2e02fa60-70b7-11eb-3332-43bd8ad21151
# ╠═cc0e2790-7094-11eb-3555-91d53a0f2791
# ╠═73c94bb8-7098-11eb-3b39-893142b5cc08
# ╠═38c6ddd4-709b-11eb-22dc-91e11f4922d2
# ╠═a1af358a-709b-11eb-194b-e32956b6c33d
# ╠═c1e6dc5e-709b-11eb-2e9f-a91753de7104
# ╠═e9d138ae-709b-11eb-38d0-6fdb86e287ee
# ╠═ecb072fa-70b5-11eb-2eea-3bc9a3836f12
