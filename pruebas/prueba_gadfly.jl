using Pkg
Pkg.add("Gadfly")
Pkg.add("RDatasets")

using Gadfly, RDatasets
set_default_plot_size(21cm, 8cm)

coord = Coord.cartesian(xmin=-2, xmax=2, ymin=-2, ymax=2)
p1 = plot(coord, z=(x,y)->x*exp(-(x^2+y^2)),
          xmin=[-2], xmax=[2], ymin=[-2], ymax=[2],
# or:     x=-2:0.25:2.0, y=-2:0.25:2.0,
          Geom.vectorfield(scale=0.4, samples=17), Geom.contour(levels=6),
          Scale.x_continuous(minvalue=-2.0, maxvalue=2.0),
          Scale.y_continuous(minvalue=-2.0, maxvalue=2.0),
          Guide.xlabel("x"), Guide.ylabel("y"), Guide.colorkey(title="z"))

volcano = Matrix{Float64}(dataset("datasets", "volcano"))
volc = volcano[1:4:end, 1:4:end]
coord = Coord.cartesian(xmin=1, xmax=22, ymin=1, ymax=16)
p2 = plot(coord, z=volc, x=1.0:22, y=1.0:16,
          Geom.vectorfield(scale=0.05), Geom.contour(levels=7),
          Scale.x_continuous(minvalue=1.0, maxvalue=22.0),
          Scale.y_continuous(minvalue=1.0, maxvalue=16.0),
          Guide.xlabel("x"), Guide.ylabel("y"),
          Theme(key_position=:none))

hstack(p1,p2)
